# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval

__all__ = ['TransportPlace', 'Route']


class TransportPlace(ModelSQL, ModelView):
    "Transport Place"
    __name__ = 'transport.place'
    name = fields.Char('Name', required=True)
    code = fields.Char('Code')

    @classmethod
    def __setup__(cls):
        super(TransportPlace, cls).__setup__()
        cls._order.insert(0, ('name', 'ASC'))


class Route(ModelSQL, ModelView):
    "Route"
    __name__ = 'transport.route'
    name = fields.Char('Name', required=True)
    service = fields.Many2One('product.product', 'Service',
                              domain=[
                                ('type', '=', 'service'),
                                ('salable', '=', True)
                                ])
    code = fields.Char('Code')
    manifest = fields.Char('Manifest')
    travel_distance = fields.Integer('Travel Distance', help='KM')
    estimated_time = fields.Numeric('Estimated Time', help='Horas', digits=(16, Eval('unit_digits', 1)))
    costs = fields.One2Many('transport.route_cost', 'route', 'Costs')
    hourly_cost = fields.Function(fields.Numeric('Hourly Cost', digits=(16, Eval('unit_digits', 2))), 'get_hourly_cost')
    total_amount = fields.Function(fields.Numeric('Cost Price', digits=(16, Eval('unit_digits', 2))), 'get_total_amount')

    @classmethod
    def __setup__(cls):
        super(Route, cls).__setup__()
        cls._order.insert(0, ('name', 'ASC'))

    def get_rec_name(self, name):
        if self.service:
            return '%s (%s)' % (self.service.name, self.name)
        else:
            return self.name

    def get_hourly_cost(self, name=None):
        total = 0
        if self.total_amount and self.travel_distance and self.travel_distance > 0:
            total = self.total_amount / self.travel_distance
        return total

    def get_total_amount(self, name=None):
        total = 0
        for cost in self.costs:
            total += cost.total_cost
        return total


class RouteCost(ModelSQL, ModelView):
    "Route Cost"
    __name__ = 'transport.route_cost'
    route = fields.Many2One('transport.route', 'Route')
    product = fields.Many2One('product.product', 'Product', required=True)
    cost_price = fields.Numeric('Cost Price', required=True,
                                digits=(2, Eval('unit_digits', 2)))
    quantity = fields.Numeric('Quantity', required=True,
                                digits=(2, Eval('unit_digits', 2)))
    total_cost = fields.Function(fields.Numeric('Total Cost', digits=(2, Eval('unit_digits', 2))), 'get_total_cost')

    def get_total_cost(self, name=None):
        if self.cost_price and self.quantity:
            return self.cost_price * self.quantity
        else:
            return 0

    @fields.depends('product', 'cost_price')
    def on_change_product(self):
        if self.product:
            self.cost_price = self.product.cost_price
